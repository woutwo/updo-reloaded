<?php
/*	
	The default controller is home,
	Adding new controllers just requires you to add a new file to the controllers-directory, with the name 'NAME.php'
	This file should contain a class with the name of that controller with only the first letter being capitalized.
	
	While testing this, make sure that you have filled in /config/database.cfg.php correctly, 
	and that you have created a table with this structure:
		title: admin
		field 1: id - int(11)
		field 2: username - varchar(255)
		field 3: password - text
	and after that visited /home/preparedatabase.html once.
	
*/
namespace Application\Controller;

class Test Extends \System\Controller 
{
	
	/*
		 The constructor isn't really neccesary in this case. 
		 But using this function could be usefull if you've got multiple functions which all require a specific model by loading that specific model and saving it as a object variable.
 	*/
	public function __construct()
	{
			
		// parent constructor
		parent::__construct();	
		
	}
	
	/* 
		This is the default method to be called, if there is no specific method given in the url.
		This controller is called by visiting /home.html or /home/index.html or the homepage itself(/).
	*/
	public function index()
	{

		/* Loads a template: views/pages/home.tpl */
        $this->layout->setTitle('Testpagina');
		$this->layout->view('test');
		
	}
}