<!DOCTYPE HTML>
<html lang="nl">
<head>
    <title>Updo.nl - {$title}</title>
    <link rel="stylesheet" href="/application/resources/style/bootstrap.min.css" />
    <link rel="stylesheet" href="/application/resources/style/default.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {$headers|indent:2:"\t"}
</head>
<body>
    <div id="container" class="container well">
        {$content}
    </div>
</body>
</html>