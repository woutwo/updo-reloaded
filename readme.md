Welkom in de hoofdmap van Updo Reloaded (versie 3.0); de nieuwe versie van updo.nl.

Updo Reloaded zal deels open source worden, GMOT-leden kunnen meehelpen aan de development van nieuwe features en verbeteringen; op deze manier hopen we Updo meer te kunnen aanpassen aan de wensen van de gebruikers.
